**Background**
The impetus to start the book club was based on several WTM members joining the team. We thought it would be a good way for everyone to get to know each other and also become familiar with the theory and philosophy and art of building a community.

**Goal**
We will go through the philosophy, approach, and best practices in the book.
In the spirit of the book, we will be identifying some action items from each discussion. We'll also be posted some though-provoking passages from the book here in the issue and discuss the following threads.